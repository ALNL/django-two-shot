from django.shortcuts import render,get_object_or_404,redirect
from .models import Receipt,ExpenseCategory,Account
from django.contrib.auth.decorators import login_required
from .forms import ReceiptForm,CategoryForm,AccountForm
# Create your views here.

@login_required
def receipt_list(request):
    receipts = Receipt.objects.filter(purchaser=request.user)
    context = {
        "receipt_list": receipts
    }
    return render(request,"receipts/list.html",context)

@login_required
def show_receipt(request,id):
    receipt = get_object_or_404(Receipt,id=id)
    context = {
        "receipt_object": receipt,
    }
    return render(request,"receipts/list.html",context)

@login_required
def create_receipt(request):
    if request.method == "POST":
        form = ReceiptForm(request.POST)
        if form.is_valid():
            receipt = form.save(False)
            receipt.purchaser = request.user
            receipt.save()
            return redirect("home")
    else:
        form = ReceiptForm()
    context = {
        "form":form,
    }
    return render(request,"receipts/create.html",context)

@login_required
def category_list(request):
    category = ExpenseCategory.objects.filter(owner=request.user)
    context = {
        "category":category
    }
    return render(request,"receipts/categories.html",context)

@login_required
def account_list(request):
    account = Account.objects.filter(owner=request.user)
    context = {
        "account":account
    }
    return render(request,"receipts/accounts.html",context)

@login_required
def create_category(request):
    if request.method == "POST":
        form = CategoryForm(request.POST)
        if form.is_valid():
            category = form.save(False)
            category.owner = request.user
            category.save()
            return redirect("category_list")
    else:
        form = CategoryForm()
    context = {
        "form":form,
    }
    return render(request,"receipts/new_category.html",context)

@login_required
def create_account(request):
    if request.method == "POST":
        form = AccountForm(request.POST)
        if form.is_valid():
            account = form.save(False)
            account.owner = request.user
            account.save()
            return redirect("account_list")
    else:
        form = AccountForm()
    context = {
        "form":form
    }
    return render(request,"receipts/new_account.html",context)
